//
//  BaseVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 25/04/20.
//  Copyright © 2020 Nikhil Jobanputra All rights reserved.
//

import UIKit
import FirebaseAnalytics

class BaseVC: UIViewController {
    
    static let sharedInstance = BaseVC()
    
    lazy var dateFormatter = DateFormatter()
    
    //MARK: - View Life Cycle -
    override func viewDidLoad(){
        
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool){
        
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool){
        
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool){
        
        super.viewWillDisappear(animated)
    }
    override func didReceiveMemoryWarning(){
        
        super.didReceiveMemoryWarning()
    }
    
    func todayDate() -> String {
        
        let date = Date()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return dateFormatter.string(from: date)
    }
    
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

    func getCurrentDateTime() -> String{
        
        let currentDate         = DateFormatter()
        currentDate.dateFormat  = "HH:mm:ss"
        currentDate.timeZone    = TimeZone.current
        return currentDate.string(from: Date())
    }
    
    func setScreenAnalytics(screenName: String, screenClassVC: String){
        
        Analytics.setScreenName(screenName, screenClass: screenClassVC)
    }
    
    func setFirebaseAnalytic(EventName: String, ID: String, ItemName: String, ContentType:String ){
        
        Analytics.logEvent(EventName, parameters: [
            AnalyticsParameterItemID: "id-\(ID)" as NSObject,
            AnalyticsParameterItemName: ItemName as NSObject,
            AnalyticsParameterContentType: ContentType as NSObject
        ])
    }
    
    //MARK: - UserDefault Operation -
    func isExistUserDefaultKey(_ key : String) -> Bool{
        
        if  (user.value(forKey: key) != nil){
            
            return true;
        }
        return false;
    }
    
    func removeUserDefaultKey(_ key : String){
        
        if  (user.value(forKey: key) != nil){
            
            user.removeObject(forKey: key)
            user.synchronize()
        }
    }
    
    func clearUserDefaultAllKey(){
        
        for key in user.dictionaryRepresentation().keys{
            
            user.removeObject(forKey: key)
        }
    }
    func showAlertWithOKButton(message: String) {
        
        let alert = UIAlertController(title: kAppName, message: message, preferredStyle: .alert)
        
        let okaction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okaction)
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertWith2Buttons(message:String, btnOneName:String, btnTwoName:String, complition:@escaping  (_ btnAction:Int) -> Void ) {
        
        let alert = UIAlertController(title: kAppName, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: btnOneName, style: .default) { (action) in
            complition(1)
        }
        alert.addAction(action1)
        
        let action2 = UIAlertAction(title: btnTwoName, style: .default) { (UIAlertActionaction2) in
            complition(2)
        }
        alert.addAction(action2)
        
        present(alert, animated: true, completion: nil)
    }
}
extension Notification.Name {
    
    static let NFCCounter      = Notification.Name("NFCCounter")
    static let NFCReadHistory  = Notification.Name("NFCReadHistory")
    static let NFCWriteHistory = Notification.Name("NFCWriteHistory")
}
