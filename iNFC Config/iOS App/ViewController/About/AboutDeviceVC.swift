//
//  AboutDeviceVC.swift
//  iNFC Config
//
//  Created by Nikhil Jobanputra on 22/03/20.
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

class AboutDeviceVC: BaseVC {

    //-------------------------------------------------------------------
    //                     MARK: - Outlet -
    //-------------------------------------------------------------------
    @IBOutlet weak var lblDevieName:UILabel!
    @IBOutlet weak var lblDevieType:UILabel!
    @IBOutlet weak var lblDevieVersion:UILabel!
    
    @IBOutlet weak var lblBluetoothVersion:UILabel!
    @IBOutlet weak var lblNFCSupport:UILabel!
    
    //-------------------------------------------------------------------
    //                     MARK: - Property -
    //-------------------------------------------------------------------
    fileprivate var nfcRead = NFCReader()
    
    //-------------------------------------------------------------------
    //                     MARK: - View Life Cycle -
    //-------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblDevieName.text        = UIDevice.current.name
        self.lblDevieType.text        = UIDevice.modelName
        self.lblDevieVersion.text     = UIDevice.current.systemVersion
        
        if self.nfcRead.isDeviceSupportNFCReading{
            
            self.lblNFCSupport.text = "Yes"
        }
    }
    
    //-------------------------------------------------------------------
    //                     MARK: - Property -
    //-------------------------------------------------------------------
    
    //-------------------------------------------------------------------
    //                     MARK: - Button Action -
    //-------------------------------------------------------------------
    @IBAction func onButtonBack(sender:UIButton) {
           
        self.dismiss(animated: true, completion: nil)
    }
}
