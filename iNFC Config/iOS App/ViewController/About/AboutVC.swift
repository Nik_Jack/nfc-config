//
//  AboutVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra on 29/04/20.
//  Copyright © 2020  Nikhil Jobanputra. All rights reserved.
//

import UIKit
import MessageUI

class AboutVC: BaseVC {
    
    //-------------------------------------------------------------------
    //                     MARK: - Outlet -
    //-------------------------------------------------------------------
    @IBOutlet weak var CatCollection: UICollectionView!
    
    //-------------------------------------------------------------------
    //                     MARK: - Property -
    //-------------------------------------------------------------------
    fileprivate var arrAboutUS = [NSDictionary]()
    private let sectionInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    private let itemsPerRow: CGFloat = 3
    
    //-------------------------------------------------------------------
    //                     MARK: - View Life Cycle -
    //-------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrAboutUS.append(["img":"ic_AUAnroid","Title":"Android App","subTitle":"We Developed in anfdf"])
        
        self.arrAboutUS.append(["img":"ic_AUApple","Title":"iPhone App","subTitle":""])
        
        self.arrAboutUS.append(["img":"ic_AUBluetooth","Title":"Bluetooth App","subTitle":""])
        
        self.arrAboutUS.append(["img":"ic_AUIbeacon","Title":"iBeacon App","subTitle":""])
        
        self.arrAboutUS.append(["img":"ic_AUIOT","Title":"IoT App","subTitle":""])
        
        self.arrAboutUS.append(["img":"ic_AUNFC","Title":"NFC App","subTitle":""])
        
        self.setScreenAnalytics(screenName: SCNAM_ABOUT_SCREEN, screenClassVC: "AboutVC")
    }
        
    //-------------------------------------------------------------------
    //                     MARK: - Button Action -
    //-------------------------------------------------------------------
    
    @IBAction func OnButtonEmail(_ sender: UIButton){
        
        let mailComposeViewController = configuredMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    @IBAction func OnButtonWebRedirect(_ sender: UIButton){
        
        guard let url = URL(string: "http://www.inikinfotech.com") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func onButtonBack(sender:UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
extension AboutVC: MFMailComposeViewControllerDelegate{
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["contact@inikinfotech.com"])
        mailComposerVC.setSubject("Reg: NFC Config iOS queries")
        mailComposerVC.setMessageBody(" ", isHTML: false)
        return mailComposerVC
    }
    func showSendMailErrorAlert() {
        
        self.showAlertWithOKButton(message: "Your device could not send e-mail.  Please check e-mail configuration and try again.")
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        controller.dismiss(animated: true, completion: nil)
    }
}
class AboutProfileCVC: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}


extension AboutVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    //MARK: - Collection
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrAboutUS.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = CatCollection.dequeueReusableCell(withReuseIdentifier: "AboutProfileCVC", for: indexPath) as! AboutProfileCVC
        let dict = self.arrAboutUS[indexPath.row]

        cell.lblTitle.text     = dict.value(forKey: "Title") as? String
        cell.iconImage.image  = UIImage(named: dict["img"] as! String)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let paddingSpace = sectionInsets.left * (itemsPerRow)
        let availableWidth = (view.frame.width - paddingSpace) - 44
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem - 16)
    }
}
