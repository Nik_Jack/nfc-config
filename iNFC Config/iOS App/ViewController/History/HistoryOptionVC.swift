//
//  HistoryOptionVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 17/02/20.
//  Copyright © 2020 Nikhil Jobanputra All rights reserved.
//

import UIKit

class HistoryOptionVC: BaseVC{
    
    //----------------------------------------------------------------------------
    //                              MARK: - Outlet -
    //----------------------------------------------------------------------------
    @IBOutlet var scollview:UIScrollView!
    @IBOutlet var lblLine:UILabel!
    @IBOutlet var btn:UIButton!
    @IBOutlet var arrBtn:[UIButton]!
    
    
    //----------------------------------------------------------------------------
    //                              MARK: - Property -
    //----------------------------------------------------------------------------
    var selectIndex = 0
    
    //----------------------------------------------------------------------------
    //                              MARK: - View Life Cycle -
    //----------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setScreenAnalytics(screenName: SCNAM_HISTORY_SCREEN, screenClassVC: "HistoryOptionVC")
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //----------------------------------------------------------------------------
    //                              MARK: - Function -
    //----------------------------------------------------------------------------
    fileprivate func selectedIndex(index:Int){
        
        for (indexId,element) in arrBtn.enumerated(){
            
            if indexId == index{
                element.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            }else{
                element.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            }
            lblLine.frame.origin.x = CGFloat(arrBtn[index].frame.width * CGFloat(index))
        }
    }
    
    //----------------------------------------------------------------------------
    //                              MARK: - Button Action -
    //----------------------------------------------------------------------------
    @IBAction func btnClick(sender:UIButton) {
        
        let size = UIScreen.main.bounds
        let x = (size.width * CGFloat(sender.tag))
        
        scollview.scrollRectToVisible(CGRect(x: x, y: 0, width: size.width, height: scollview.frame.height), animated: true)
    }
    @IBAction func onButtonBack(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onButtonDelete(sender:UIButton) {
        
        self.showAlertWith2Buttons(message: ALERT_DELETE, btnOneName: ALERT_YES, btnTwoName: ALERT_NO) { (value) in
            if value == 1{
                
                if self.selectIndex == 1{ //WriteHistory
                    
                    NotificationCenter.default.post(name: .NFCWriteHistory, object: true, userInfo: nil)
                } else{
                    
                    NotificationCenter.default.post(name: .NFCReadHistory, object: true, userInfo: nil)
                }
                NotificationCenter.default.post(name: .NFCCounter, object: nil, userInfo: nil)
            }
        }
    }
}
extension HistoryOptionVC:UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentPage = Int((scrollView.contentOffset.x + (0.5 * scrollView.frame.size.width)) / scrollView.frame.width)
        self.selectIndex = currentPage
        self.selectedIndex(index: currentPage)
    }
}

