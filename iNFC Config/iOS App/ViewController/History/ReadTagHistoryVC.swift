//
//  ReadHistoryVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra on 26/04/20.
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

class ReadTagHistoryVC: BaseVC {
    
    //---------------------------------------------------
    //                   MARK: - Outlet -
    //---------------------------------------------------
    @IBOutlet var tblShow:UITableView!
    @IBOutlet var lblNoData:UILabel!
    @IBOutlet var backView:UIView!
    
    
    //---------------------------------------------------
    //                   MARK: - Property -
    //---------------------------------------------------
    fileprivate var arrOfRead:[UserData] = []
    
    //---------------------------------------------------
    //                   MARK: - View Life Cycle -
    //---------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCell()
        self.fetchLocalDBData()
        NotificationCenter.default.removeObserver(self, name: .NFCReadHistory, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReadNFCHistory), name: .NFCReadHistory, object: nil)
    }
    
    //---------------------------------------------------
    //                   MARK: - Function -
    //---------------------------------------------------
    private func registerCell(){
        
        tblShow.register(ReadTagTVC.nib, forCellReuseIdentifier: ReadTagTVC.defaultReuseIdentifier)
    }
    
    private func fetchLocalDBData() {
        
        _ = DBManager.getNFCWriteData()
        self.arrOfRead = DBManager.getNFCReadData()
        if self.arrOfRead.count > 0{
            
            self.lblNoData.isHidden   = true
            self.backView.isHidden     = false
            DispatchQueue.main.async {
                
                self.tblShow.reloadData()
            }
        }else{
            self.lblNoData.isHidden   = false
            self.backView.isHidden    = true
        }
    }
    @objc func ReadNFCHistory(notification: Notification) {
        
        let bleHistory = notification.object as? Bool
        
        if bleHistory!{
            
            if DBManager.deleteAllNFC(tblName: ktbl_NFCReadData){
                
                self.lblNoData.isHidden   = false
                self.backView.isHidden    = true
            }
        }
    }
}

//MARK:- Tableview Datasource
extension ReadTagHistoryVC:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrOfRead.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblShow.dequeueCell(with: ReadTagTVC.self)
        
        cell.selectionStyle = .none
        let NFCData         = self.arrOfRead[indexPath.row]
        cell.lblType.text = NFCData.type ?? ""
        cell.lblText.text = NFCData.readstring ?? ""
        cell.lblDate.text = NFCData.datetime ?? ""
        
        if NFCData.type == writeType.Weblink.type() {
            
            cell.imgIcon.image = UIImage(named: "ic_url")
        } else {
            
            cell.imgIcon.image = UIImage(named: "ic_text")
        }
        return cell
    }
}
extension UITableViewCell{

    public static var defaultReuseIdentifier:String{
        
        return "\(self)"
    }
    
    public static var nib: UINib {
        
        return UINib.init(nibName: self.defaultReuseIdentifier, bundle: nil)
    }
}
extension UITableView{
    
    ///Dequeue Table View Cell
    func dequeueCell <T: UITableViewCell> (with identifier: T.Type) -> T {
        
        return self.dequeueReusableCell(withIdentifier: "\(identifier.self)") as! T
    }
}
