//
//  WriteTagHistoryVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra on 26/04/20.
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

class WriteTagHistoryVC: BaseVC {
    
    //-------------------------------------------------------------------
    //                     MARK: - Outlet -
    //-------------------------------------------------------------------
    @IBOutlet var tblShow:UITableView!
    @IBOutlet var lblNoData:UILabel!
    @IBOutlet var backView:UIView!
    
    //-------------------------------------------------------------------
    //                     MARK: - Property -
    //-------------------------------------------------------------------
    fileprivate var arrOfWrite:[UserData] = []
    
    //-------------------------------------------------------------------
    //                     MARK: - View Life Cycle -
    //-------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCell()
        self.fetchLocalDBData()
        NotificationCenter.default.removeObserver(self, name: .NFCWriteHistory, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.WriteNFCHistory), name: .NFCWriteHistory, object: nil)
        
    }
    
    //-------------------------------------------------------------------
    //                     MARK: - Function -
    //-------------------------------------------------------------------
    private func registerCell(){
        
        self.tblShow.register(ReadTagTVC.nib, forCellReuseIdentifier: ReadTagTVC.defaultReuseIdentifier)
    }
    
    private func fetchLocalDBData() {
        
        self.arrOfWrite = DBManager.getNFCWriteData()
        if self.arrOfWrite.count > 0{
            
            self.lblNoData.isHidden   = true
            self.backView.isHidden     = false
            DispatchQueue.main.async {
                
                self.tblShow.reloadData()
            }
        }else{
            self.lblNoData.isHidden   = false
            self.backView.isHidden    = true
        }
    }
    @objc func WriteNFCHistory(notification: Notification) {
        
        let bleHistory = notification.object as? Bool
        
        if bleHistory!{
            
            if DBManager.deleteAllNFC(tblName: ktbl_NFCWriteData){
                
                self.lblNoData.isHidden   = false
                self.backView.isHidden    = true
            }
        }
    }
}

//MARK:- Tableview Datasource
extension WriteTagHistoryVC:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return self.arrOfWrite.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblShow.dequeueCell(with: ReadTagTVC.self)
        cell.selectionStyle    = .none
        let NFCData = self.arrOfWrite[indexPath.row]
        
        cell.lblType.text = NFCData.type ?? ""
        cell.lblText.text = NFCData.writestring ?? ""
        cell.lblDate.text = NFCData.datetime ?? ""
        if NFCData.type == writeType.Weblink.type() {
            
            cell.imgIcon.image = UIImage(named: "ic_url")
        } else {
            
            cell.imgIcon.image = UIImage(named: "ic_text")
        }
        return cell
    }
}
