//
//  ReadTagVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 16/02/20.
//  Copyright © 2020 Nikhil Jobanputra All rights reserved.
//

import UIKit

class ReadTagVC: BaseVC {
    
    //---------------------------------------------------
    //                  MARK: - Outlet -
    //---------------------------------------------------
    @IBOutlet var tblShow:UITableView!
    @IBOutlet var noNRFData:UIView!
    @IBOutlet var listTagView:UIView!
    
    //---------------------------------------------------
    //                 MARK: - Property -
    //---------------------------------------------------
    fileprivate var arrOfRead:[UserData] = []
    
    //---------------------------------------------------
    //                 MARK: - View Life Cycle -
    //---------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setScreenAnalytics(screenName: SCNAM_READ_TAG_SCREEN, screenClassVC: "ReadTagVC")
        self.fetchLocalDBData()
    }
    
    //---------------------------------------------------
    //                 MARK: - Function -
    //---------------------------------------------------
    private func fetchLocalDBData() {
        
        _ = DBManager.getNFCWriteData()
        self.arrOfRead = DBManager.getNFCReadData()
        if self.arrOfRead.count > 0{
            
            self.noNRFData.isHidden   = true
            self.listTagView.isHidden = false
            DispatchQueue.main.async {
                
                self.tblShow.reloadData()
            }
        }else{
            self.noNRFData.isHidden   = false
            self.listTagView.isHidden = true
        }
    }
    
    //---------------------------------------------------
    //                 MARK: - Button Action -
    //----------------------------------------------------
    @IBAction func onButtonBack(sender:UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onButtonRead(sender:UIButton) {
        
        if nfcReader.isDeviceSupportNFCReading{
            
            nfcReader.readNFC { (readString, error) in
                if error == nil {
                    
                    let currentDate         = DateFormatter()
                    currentDate.dateFormat  = "dd-MMM-yyyy HH:mm:ss"
                    currentDate.timeZone    = TimeZone.current
                    let dateTime = currentDate.string(from: Date())
                    
                    let readData = UserData()
                    
                    let data = arrofWriteData.firstIndex { (userData) -> Bool in
                        return userData.writestring == readString
                    }
                    readData.readstring = readString
                    
                    if readData.type == writeType.BusinessCard.type() {
                        //readData.readstring = "\(readString)"
                    } else {
                        if data != nil {
                            readData.type = arrofWriteData[data!].type
                        } else {
                            if readString!.contains("http") {
                                
                                readData.type = writeType.Weblink.type()
                            }else if readString!.isAllDigits(){
                                
                                readData.type = writeType.TelephoneNumber.type()
                            }else{
                                readData.type = writeType.Text.type()
                            }
                        }
                    }
                    _ = DBManager.insertReadData(type: readData.type!, readstring: readString!, datetime: dateTime)
                    self.fetchLocalDBData()
                    NotificationCenter.default.post(name: .NFCCounter, object: nil, userInfo: nil)
                }
            }
        }else{
            
            self.showAlertWithOKButton(message: "Device does not support NFC")
        }
    }
}

//MARK:- Tableview Datasource
extension ReadTagVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrOfRead.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReadTagTVC") as! ReadTagTVC
        cell.selectionStyle    = .none
        
        let NFCData = self.arrOfRead[indexPath.row]
        
        cell.lblDate.text = NFCData.datetime ?? ""
        cell.lblType.text = NFCData.type ?? ""
        cell.lblText.text = NFCData.readstring ?? ""
        if NFCData.type == writeType.Weblink.type() {
            
            cell.imgIcon.image = UIImage(named: "ic_url")
        } else {
            
            cell.imgIcon.image = UIImage(named: "ic_text")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let more = UITableViewRowAction(style: .default, title: "Delete") { action, index in
            
            if self.arrOfRead.count > 0{
                
                let NFCData = self.arrOfRead[indexPath.row]
                tableView.beginUpdates()
                if DBManager.deleteAllNFCRow(tblName: ktbl_NFCReadData, id: NFCData.id!){
                    
                    self.arrOfRead.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                    tableView.endUpdates()
                    if self.arrOfRead.count > 0{
                        
                        self.noNRFData.isHidden   = true
                        self.listTagView.isHidden = false
                    }else{
                        self.noNRFData.isHidden   = false
                        self.listTagView.isHidden = true
                    }
                    print("Delete button tapped:->\(index)")
                }
            }else{
                
                self.noNRFData.isHidden   = false
                self.listTagView.isHidden = true
            }
        }
        more.backgroundColor = UIColor.red
        
        return [more]
    }
    private func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
        }
    }
    
    private func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell.EditingStyle {
        
        return UITableViewCell.EditingStyle.delete
    }
}
