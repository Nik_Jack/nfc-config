//
//  WriteTagVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 17/02/20.
//  Copyright © 2020 Nikhil Jobanputra All rights reserved.
//

import UIKit

class WriteTagVC: BaseVC {
    
    //---------------------------------------------------
    //                   MARK: - Outlet -
    //---------------------------------------------------
    @IBOutlet weak var cblTagOption: UICollectionView!
    @IBOutlet weak var tblWriteTag:UITableView!
    
    //---------------------------------------------------
    //                   MARK: - Property -
    //---------------------------------------------------
    fileprivate var arrWriteTag    = [NSDictionary]()
    fileprivate var arrWriteOption = [NSDictionary]()
    lazy var arrWrite:[UserData]   = []
    
    //---------------------------------------------------
    //                  MARK: - View Life Cycle -
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.dateFormatter.dateFormat  = "dd-MMM-yyyy HH:mm:ss"
        self.dateFormatter.timeZone    = TimeZone.current
        
        self.arrWriteOption.append(["imgicon":"ic_text", "Title":"Text", "color": UIColor.init(red: 227.0/255.0, green: 255.0/255.0, blue: 236.0/255.0, alpha: 1.0)])
        self.arrWriteOption.append(["imgicon":"ic_url", "Title":"URL", "color": UIColor.init(red: 172.0/255.0, green: 229.0/255.0, blue: 238.0/255.0, alpha: 1.0)])
        
        self.arrWriteTag.append(["imgBG":"ic_text","Title":"Read Tag"])
        self.arrWriteTag.append(["imgBG":"ic_url","Title":"Read Tag"])
        
        self.setScreenAnalytics(screenName: SCNAM_WRITE_TAG_SCREEN, screenClassVC: "WriteTagVC")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.featchDBLocalData()
    }
    
    //---------------------------------------------------
    //                     MARK: - Function -
    //---------------------------------------------------
    private func featchDBLocalData() {
        
        self.arrWrite = DBManager.getNFCWriteData()
        if self.arrWrite.count > 0{
            
            self.tblWriteTag.isHidden  = false
            DispatchQueue.main.async {
                
                self.tblWriteTag.reloadData()
            }
        }else{
            self.tblWriteTag.isHidden = true
        }
    }

    
    private func getDailogText(vc: WriteDataVC) {
        
        vc.dailogData(myComplition: { (dataType, textone, textTwo) in
            
            let dataModel = UserData()
            dataModel.type = "\(dataType)"
            dataModel.writestring = textone
            dataModel.isselected = 1
            let dateTime = self.dateFormatter.string(from: Date())
            
            switch dataType{
                
            case .Text:
                _ = DBManager.insertWriteData(type: "\(dataType)", writestring: textone, datetime: dateTime, contectName: "test", contectNumber: "test", isselected: 1)
                break;
            case .Weblink:
                let stringValue = textone + textTwo!
                _ = DBManager.insertWriteData(type: "\(dataType)", writestring: stringValue, datetime: dateTime, contectName: "test", contectNumber: "test", isselected: 1)
                break;
            case .BusinessCard:
                break;
            case .Shortcut:
                break;
            case .Location:
                break;
            case .TelephoneNumber:
                break;
            case .SMS:
                break;
            case .none:
                break;
            }
            self.featchDBLocalData()
            NotificationCenter.default.post(name: .NFCCounter, object: nil, userInfo: nil)
        })
    }
    
    //---------------------------------------------------
    //                 MARK: - Button Action -
    //---------------------------------------------------
    @IBAction func onButtonBack(sender:UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onButtonWrite(sender:UIButton) {
        
        if nfcReader.isDeviceSupportNFCReading{
            
            var arrWriteData:[UserData] = []
            
            if arrWrite.count != 0 {
                
                for (_,val) in arrWrite.enumerated() {
                    
                    if (val.isselected == 1) {
                        
                        arrWriteData.append(val)
                    }
                }
                if arrWriteData.count != 0 {
                    
                    nfcReader.writeNFC(arrofwriteData: arrWriteData) { (Writestring, error) in
                        
                        if error == nil {
                            
                            print(Writestring ?? "")
                        }
                    }
                }
            }
        }else{
            
            self.showAlertWithOKButton(message: "Device does not support NFC")
        }
    }
}
//---------------------------------------------------
//              MARK: - UICollectionViewDelegateFlowLayout -
//---------------------------------------------------
extension WriteTagVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width - 32) / 2, height: 50)
    }
}

//---------------------------------------------------
//                  MARK: - UICollectionViewDataSource -
//---------------------------------------------------
extension WriteTagVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        return self.arrWriteOption.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WriteOptionCVC", for: indexPath) as! WriteOptionCVC
        let dict = self.arrWriteOption[indexPath.row]
        
        cell.lblTitle.text =  dict.value(forKey: "Title") as? String
        cell.bgView.backgroundColor = dict.value(forKey: "color") as? UIColor
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let writeDataVC = STORY_BOARD.instantiateViewController(withIdentifier: "WriteDataVC") as! WriteDataVC
        switch indexPath.row {
        case 0:
            self.setFirebaseAnalytic(EventName: LOGNAM_WRITE_TEXT, ID: LOGID_WRITE_TEXT_ID, ItemName: LOGNAM_WRITE_TEXT, ContentType: "Select")
            writeDataVC.type = writeType.Text
            self.getDailogText(vc: writeDataVC)
            break
        case 1:
            self.setFirebaseAnalytic(EventName: LOGNAM_WRITE_URL, ID: LOGID_WRITE_URL_ID, ItemName: LOGNAM_WRITE_URL, ContentType: "Select")
            writeDataVC.type = writeType.Weblink
            self.getDailogText(vc: writeDataVC)
            break
        default:
            print("Default selectoin")
        }
        self.present(writeDataVC, animated: true, completion: nil)
    }
}
extension WriteTagVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return self.arrWrite.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WriteTagTableViewCell") as! WriteTagTableViewCell
        
        let NFCData = self.arrWrite[indexPath.row]
        
        cell.lblDate.text = NFCData.datetime ?? ""
        cell.lblType.text = NFCData.type ?? ""
        cell.lblText.text = NFCData.writestring ?? ""
        if NFCData.type == writeType.Weblink.type() {
            
            cell.imgIcon.image = UIImage(named: "ic_url")
        } else {
            
            cell.imgIcon.image = UIImage(named: "ic_text")
        }
        if NFCData.isselected == 1 {
            
            cell.imgSelected.isHidden = false
        } else {
            cell.imgSelected.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrWrite[indexPath.row].isselected == 1 {
            
            if DBManager.updateNFCData(iselected: 0, id: arrWrite[indexPath.row].id!){
                
                arrWrite[indexPath.row].isselected = 0
            }
        } else {
            
            if DBManager.updateNFCData(iselected: 1, id: arrWrite[indexPath.row].id!){
                
                arrWrite[indexPath.row].isselected = 1
            }
        }
        DispatchQueue.main.async {
            
            self.tblWriteTag.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let more = UITableViewRowAction(style: .default, title: "Delete") { action, index in
            
            if self.arrWrite.count > 0{
                
                let NFCData = self.arrWrite[indexPath.row]
                tableView.beginUpdates()
                if DBManager.deleteAllNFCRow(tblName: ktbl_NFCWriteData, id: NFCData.id!){
                    
                    self.arrWrite.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
                    tableView.endUpdates()
                    NotificationCenter.default.post(name: .NFCCounter, object: nil, userInfo: nil)
                    print("Delete button tapped:->\(index)")
                }
            }
        }
        more.backgroundColor = UIColor.red
        return [more]
    }
    private func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            
        }
    }
    private func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell.EditingStyle {
        
        return UITableViewCell.EditingStyle.delete
    }
}
class WriteOptionCVC: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
}
