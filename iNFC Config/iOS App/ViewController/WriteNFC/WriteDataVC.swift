//
//  WriteDataVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra on 26/04/20.
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

let writeDataVC = WriteDataVC()

class WriteDataVC: BaseVC {
    
    //---------------------------------------------------
    //                     MARK: - Outlet -
    //---------------------------------------------------
    @IBOutlet weak var txtUserInput: UITextField!
    @IBOutlet weak var txtWebInput: UITextField!
    @IBOutlet weak var lblNavTitle:UILabel!
    @IBOutlet weak var lblWriteTitle:UILabel!
    @IBOutlet weak var txtWebInputHeight:NSLayoutConstraint!
    
    //---------------------------------------------------
    //                     MARK: - Property -
    //---------------------------------------------------
    typealias dataComplition = ((writeType,String,String?) -> Void)
    var typeComplition:dataComplition?
    lazy var dataType = writeType.none
    
    lazy var type = writeType.none
    var pickerView: UIPickerView!
    
    lazy var webInputArray = ["http://www.","https://www.","http://","https://"]
    
    //---------------------------------------------------
    //                     MARK: - View Life Cycle -
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pickerView = UIPickerView()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.showsSelectionIndicator = true
        self.txtWebInput.inputView = pickerView
        
        switch type {
        
        case writeType.Weblink :
            self.lblWriteTitle.text = "Write URL"
            self.txtWebInput.text = "Cretae WebLink"
            self.txtWebInput.text = self.webInputArray[0]
            dataType = writeType.Weblink
            break
        case writeType.Text :
            self.txtWebInputHeight.constant = 0
            self.lblWriteTitle.text = "Write Text"
            self.lblNavTitle.text = "Create Text"
            self.txtUserInput.placeholder = "Type text"
            dataType = writeType.Text
            break
        default:
            break
        }
        
        self.setScreenAnalytics(screenName: SCNAM_WRITE_SELECTION_SCREEN, screenClassVC: "WriteDataVC")
    }
    
    func dailogData( myComplition:@escaping dataComplition) {
        
        typeComplition = myComplition
    }
    
    //---------------------------------------------------
    //                     MARK: - Button Action -
    //---------------------------------------------------
    @IBAction func onButtonBack(sender:UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onButtonSave(sendr:UIButton) {
        
        if dataType == writeType.Text {
            
            if(self.txtUserInput.text!.isEmpty){
                //print("Please enter some text")
            } else{
                typeComplition!(dataType,(self.txtUserInput.text)!, nil)
            }
        } else {
            if(self.txtUserInput.text!.isEmpty){
                //print("Please enter some text")
            } else{
                typeComplition!(dataType,(self.txtWebInput.text)!, self.txtUserInput.text!)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension WriteDataVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if self.txtWebInput.isFirstResponder{
            
            return self.webInputArray[row]
        } else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if self.txtWebInput.isFirstResponder{
            
            self.txtWebInput.text = self.webInputArray[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.txtWebInput.isFirstResponder{
            
            if self.webInputArray.count > 0{
                
                return self.webInputArray.count
            } else{
                return 0
            }
        } else{ return 0}
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        if self.txtWebInput.isFirstResponder{
            
            if self.webInputArray.count > 0{
                
                return NSAttributedString(string: self.webInputArray[row], attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            } else{
                
                return NSAttributedString(string: "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            }
        } else{
            return NSAttributedString(string: "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
}
