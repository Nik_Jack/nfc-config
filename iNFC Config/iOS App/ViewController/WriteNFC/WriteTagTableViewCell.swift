//
//  WriteTagTableViewCell.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 17/02/20.
//  Copyright © 2020 Nikhil Jobanputra All rights reserved.
//

import UIKit

class WriteTagTableViewCell: UITableViewCell {

    @IBOutlet var imgIcon:UIImageView!
    @IBOutlet var lblType:UILabel!
    @IBOutlet var lblText:UILabel!
    @IBOutlet var lblDate:UILabel!
    @IBOutlet var imgSelected:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}



