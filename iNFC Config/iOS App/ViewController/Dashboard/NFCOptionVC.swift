//
//  NFCOptionVC.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 16/02/20.
//  Copyright © 2020 Nikhil Jobanputra All rights reserved.
//

import UIKit

class NFCOptionVC: BaseVC {
    
    //-------------------------------------------------------------------
    //                     MARK: - Outlet -
    //-------------------------------------------------------------------
    @IBOutlet var cblNFC:UICollectionView!
    
    //-------------------------------------------------------------------
    //                     MARK: - Property -
    //-------------------------------------------------------------------
    fileprivate var arrTagOption = [NSDictionary]()
    
    //-------------------------------------------------------------------
    //                     MARK: - View Life Cycle -
    //-------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setScreenAnalytics(screenName: SCNAM_HOME_SCREEN, screenClassVC: "NFCOptionVC")
        self.fillArray()
    }
    
    //-------------------------------------------------------------------
    //                     MARK: - Function -
    //-------------------------------------------------------------------
    fileprivate func fillArray(){
        
        self.arrTagOption.append(["imgBG":"ic_red_square","img_icon":"ic_read_tag","Title":"Read Tag"])
        self.arrTagOption.append(["imgBG":"ic_write_square","img_icon":"ic_write_tag","Title":"Write Tag"])
        self.arrTagOption.append(["imgBG":"ic_history_square","img_icon":"ic_history","Title":"History"])
        self.arrTagOption.append(["imgBG":"ic_about_square","img_icon":"ic_about","Title":"About Phone"])
    }
}

//MARK:- collectionview datasource
extension NFCOptionVC:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrTagOption.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "iNFCConfigCVC", for: indexPath) as! NFCConfigCVC
        
        let dict = self.arrTagOption[indexPath.row]
        cell.imgBackground.image = UIImage(named: dict.value(forKey: "imgBG") as! String)
        cell.imgIcon.image       = UIImage(named: dict.value(forKey: "img_icon") as! String)
        cell.lblName.text        = dict.value(forKey: "Title") as? String
        return cell
    }
}

//MARK:- collectionview delegate

extension NFCOptionVC:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row{

        case 0:
            self.setFirebaseAnalytic(EventName: LOGNAM_NFC_READ, ID: LOGID_NFC_READ_ID, ItemName: LOGNAM_NFC_READ, ContentType: "Click")
            let readTagVC = storyboard?.instantiateViewController(withIdentifier: "ReadTagVC") as! ReadTagVC
            self.navigationController?.pushViewController(readTagVC, animated: true)
            break;
            
        case 1:
            self.setFirebaseAnalytic(EventName: LOGNAM_NFC_WRITE, ID: LOGID_NFC_WRITE_ID, ItemName: LOGNAM_NFC_WRITE, ContentType: "Click")
            let writeTagVC = storyboard?.instantiateViewController(withIdentifier: "WriteTagVC") as! WriteTagVC
            self.navigationController?.pushViewController(writeTagVC, animated: true)
            break;
            
        case 2:
            self.setFirebaseAnalytic(EventName: LOGNAM_NFC_HISTORY, ID: LOGID_NFC_HISTORY_ID, ItemName: LOGNAM_NFC_HISTORY, ContentType: "Click")
            let historyOptionVC = storyboard?.instantiateViewController(withIdentifier: "HistoryOptionVC") as! HistoryOptionVC
            self.navigationController?.pushViewController(historyOptionVC, animated: true)
            break;
            
        case 3:
            self.setFirebaseAnalytic(EventName: LOGNAM_NFC_ABOUT, ID: LOGID_NFC_ABOUT_ID, ItemName: LOGNAM_NFC_ABOUT, ContentType: "Click")
            //let aboutVC = storyboard?.instantiateViewController(withIdentifier: "AboutDeviceVC") as! AboutDeviceVC
            let aboutVC = storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
            self.navigationController?.pushViewController(aboutVC, animated: true)
            break;

        default:
            print("")
        }
    }
}

//MARK:- collectionview delegateflowlayout
extension NFCOptionVC:UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return (CGSize(width: (collectionView.frame.width - 16) / 2, height: (collectionView.frame.width ) / 3))
    }
}

class NFCConfigCVC: UICollectionViewCell {
    
    @IBOutlet var imgBackground:UIImageView!
    @IBOutlet var imgIcon:UIImageView!
    @IBOutlet var lblName:UILabel!
    
}
