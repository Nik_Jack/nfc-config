//
//  DashboardVC.swift
//  NFC Link
//
//  Created by Nikhil Jobanputra on 21/08/20.
//  Copyright © 2021 Nikhil Jobanputra. All rights reserved.
//

import UIKit
import MessageUI

class DashboardVC: BaseVC {
    
    //---------------------------------------------------
    //                 MARK: - Outlet -
    //---------------------------------------------------
    @IBOutlet weak var CatCollection: UICollectionView!
    @IBOutlet weak var lblReadCounter: UILabel!
    @IBOutlet weak var lblWriteCounter: UILabel!
    
    //---------------------------------------------------
    //                 MARK: - Property -
    //---------------------------------------------------
    fileprivate var arrAboutUS = [NSDictionary]()
    private let sectionInsets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    private let itemsPerRow: CGFloat = 3
    
    //---------------------------------------------------
    //                     MARK: - View Life Cycle -
    //---------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrAboutUS.append(["img":"ic_read_tag","Title":"Read NFC","subTitle":"We Developed in anfdf"])
        
        self.arrAboutUS.append(["img":"ic_write_tag","Title":"Write NFC","subTitle":""])
        
        self.arrAboutUS.append(["img":"ic_history","Title":"History","subTitle":""])
        
        self.DisplayNFCData()
        NotificationCenter.default.removeObserver(self, name: .NFCCounter, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.NFCCounterData), name: .NFCCounter, object: nil)
    }
    @objc func NFCCounterData(notification: Notification) {
        
        self.DisplayNFCData()
    }
    private func DisplayNFCData() {
        
        let counterData = DBManager.getNFCCountData()
        self.lblReadCounter.text = "\(counterData.0)"
        self.lblWriteCounter.text = "\(counterData.1)"
    }
    
    //---------------------------------------------------
    //               MARK: - Button Action -
    //---------------------------------------------------
    @IBAction func onButtonAboutPhone(_ : UIButton) {
        
        let aboutVC = storyboard?.instantiateViewController(withIdentifier: "AboutDeviceVC") as! AboutDeviceVC
        self.present(aboutVC, animated: true, completion: nil)
    }
}
class DashboardCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}


extension DashboardVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrAboutUS.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = CatCollection.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath) as! DashboardCell
        let dict = self.arrAboutUS[indexPath.row]

        cell.lblTitle.text     = dict.value(forKey: "Title") as? String
        cell.iconImage.image  = UIImage(named: dict["img"] as! String)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let paddingSpace = sectionInsets.left * (itemsPerRow)
        let availableWidth = (view.frame.width - paddingSpace) - 44
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem - 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row{

        case 0:
            let readTagVC = storyboard?.instantiateViewController(withIdentifier: "ReadTagVC") as! ReadTagVC
            self.navigationController?.present(readTagVC, animated: true, completion: nil)
            break;
            
        case 1:
            let writeTagVC = storyboard?.instantiateViewController(withIdentifier: "WriteTagVC") as! WriteTagVC
            self.navigationController?.present(writeTagVC, animated: true, completion: nil)
            break;
            
        case 2:
            let historyOptionVC = storyboard?.instantiateViewController(withIdentifier: "HistoryOptionVC") as! HistoryOptionVC
            self.navigationController?.present(historyOptionVC, animated: true, completion: nil)
            break;
    
        default:
            print("")
        }
    }
}
