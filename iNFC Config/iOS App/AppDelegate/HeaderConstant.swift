//
//  HeaderConstant.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra 18/04/20.
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.

import UIKit
import Foundation

class HeaderConstant: BaseVC  {
    
    //MARK: - View Life Cycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//Log Event ID
let LOGID_WRITE_TEXT_ID     = "1"
let LOGID_WRITE_URL_ID      = "2"
let LOGID_READ_HISTORY_ID   = "3"
let LOGID_WRITE_HISTORY_ID  = "4"
let LOGID_CONTACT_US_ID     = "5"


let LOGID_NFC_READ_ID      = "3"
let LOGID_NFC_WRITE_ID     = "4"
let LOGID_NFC_HISTORY_ID   = "5"
let LOGID_NFC_ABOUT_ID     = "6"


//Log Event Name
let LOGNAM_WRITE_TEXT       = "Text_Write"
let LOGNAM_WRITE_URL        = "URL_Write"
let LOGNAM_READ_HISTORY     = "Read_History"
let LOGNAM_WRITE_HISTORY    = "Write_History"
let LOGNAM_CONTACT_US       = "Contact_US"

let LOGNAM_NFC_READ         = "ReadOption"
let LOGNAM_NFC_WRITE        = "WriteOption"
let LOGNAM_NFC_HISTORY      = "HistoryOption"
let LOGNAM_NFC_ABOUT        = "AboutOption"

//Screen Name
let SCNAM_HOME_SCREEN               = "Home_Screen"
let SCNAM_READ_TAG_SCREEN           = "Read_Tag_Screen"
let SCNAM_WRITE_TAG_SCREEN          = "Write_Tag_Screen"
let SCNAM_WRITE_SELECTION_SCREEN    = "Write_Selection_Screen"
let SCNAM_HISTORY_SCREEN            = "History_Screen"
let SCNAM_ABOUT_SCREEN              = "About_Screen"


let kAppName         = "NFC Config"

let getVersion       = "CFBundleVersion"
let getVersionString = "CFBundleShortVersionString"

let fileManager = FileManager.default

let ALERT_DELETETITLE         = "history"

//NFC Config
let ALERT_DELETE               = "Are you sure want to remove history!!!"
let ALERT_NO      = "No"
let ALERT_YES     = "Yes"
let ALERT_DISMISS = "Dismiss"


//MARK: - Storyborad Identifier -
let appDelegate     = UIApplication.shared.delegate as! AppDelegate

var STORY_BOARD: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)


//MARK: - UserDefault Key -
let user = UserDefaults.standard

enum writeType:String {
    
    case BusinessCard = ""
    case Shortcut
    case Location
    case Weblink
    case Text
    case TelephoneNumber
    case SMS
    case none
    
    func type() ->String { return self.rawValue }
}


extension Bundle {
    
    class var applicationVersionNumber: String {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        return "Version Number Not Available"
    }
    
    class var applicationBuildNumber: String {
        if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            return build
        }
        return "Build Number Not Available"
    }
}
extension String {
    
    func isAllDigits() -> Bool {
        
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}
