//
//  DBManager.swift
//  NFC Config
//
//  Created by Nikhil Jobanputra on 10/12/19.
//  Copyright © 2019 Nikhil Jobanputra. All rights reserved.
//

import Foundation

let ktbl_NFCReadData     = "tbl_NFCReadData"
let ktbl_NFCWriteData    = "tbl_NFCWriteData"

//tbl_History
let kdatetime            = "datetime"

//tbl_NFC
let kid            = "id"
let ktype          = "type"
let kreadstring    = "readstring"
let kwritestring   = "writestring"
let kcontectName   = "contectName"
let kcontectNumber = "contactNumber"
let kisselected    = "isselected"

var arrOfUserData  = [UserData]()
var arrofWriteData = [UserData]()

class UserData: NSObject {
    
    var id:Int?
    var type:String?
    var readstring:String?
    var writestring:String?
    var datetime:String?
    var contectName:String?
    var contectNumber:String?
    var isselected:Int?
}

class DBManager {
    
    //NFC Tag
    class func insertReadData(type:String,readstring:String,datetime:String) -> Bool {
        
        let query = "INSERT OR REPLACE INTO \(ktbl_NFCReadData)  (\(ktype),\(kreadstring),\(kdatetime)) VALUES(?,?,?)"
        
        let param = [type, readstring, datetime] as [Any]
        let rcs = SQLiteDB.shared.execute(sql: query, parameters: param as [AnyObject])
        
        return rcs != 0
    }
    
    class func insertWriteData(type:String, writestring:String, datetime:String, contectName:String, contectNumber:String, isselected:Int) -> Bool {
        
        let query = "INSERT OR REPLACE INTO \(ktbl_NFCWriteData)  (\(ktype),\(kwritestring),\(kdatetime),\(kcontectName),\(kcontectNumber),\(kisselected)) VALUES(?,?,?,?,?,?)"
        
        let param = [type, writestring, datetime,contectName,contectNumber,isselected] as [Any]
        let rcs = SQLiteDB.shared.execute(sql: query, parameters: param as [AnyObject])
        
        return rcs != 0
    }
    
    
    
    class func getNFCCountData() -> (Int, Int) {
        
        var query = "SELECT * FROM \(ktbl_NFCReadData)"
        let readValue = SQLiteDB.shared.query(sql: query)
        
        query = "SELECT * FROM \(ktbl_NFCWriteData)"
        let writeValue = SQLiteDB.shared.query(sql: query)
        
        return (readValue.count, writeValue.count)
    }
    
    class func getNFCReadData() -> Int{
        
        let query = "SELECT * FROM \(ktbl_NFCReadData)"
        let readValue = SQLiteDB.shared.query(sql: query)
        return readValue.count
    }
    
    
    class func getNFCReadData() -> [UserData] {
        
        arrOfUserData.removeAll()
        let query = "SELECT * FROM \(ktbl_NFCReadData)"
        let readValue = SQLiteDB.shared.query(sql: query)
        
        for userdata in readValue {
            let readData = UserData()
            readData.id = (userdata[kid] as! Int)
            readData.type = (userdata[ktype] as! String)
            readData.readstring = (userdata[kreadstring] as! String)
            readData.datetime = (userdata[kdatetime] as! String)
            
            arrOfUserData.append(readData)
        }
        arrOfUserData.reverse()
        return arrOfUserData
    }
    
    class func getNFCWriteData() -> [UserData] {
        
        arrofWriteData.removeAll()
        let query = "SELECT * FROM \(ktbl_NFCWriteData)"
        let readValue = SQLiteDB.shared.query(sql: query)
        
        for userdata in readValue {
            let writeData = UserData()
            writeData.id = (userdata[kid] as! Int)
            writeData.type = (userdata[ktype] as! String)
            writeData.writestring = (userdata[kwritestring] as! String)
            writeData.datetime = (userdata[kdatetime] as! String)
            writeData.contectName = (userdata[kcontectName] as! String)
            writeData.contectNumber = (userdata[kcontectNumber] as! String)
            writeData.isselected = (userdata[kisselected] as! Int)
            
            arrofWriteData.append(writeData)
        }
        arrofWriteData.reverse()
        return arrofWriteData
    }
    
    class func updateNFCData(iselected: Int, id: Int) -> Bool {
        
        let query = "UPDATE \(ktbl_NFCWriteData) SET \(kisselected) = '\(iselected)' WHERE \(kid) = '\(id)'"
        
        let rcs = SQLiteDB.shared.execute(sql: query)
        
        return rcs != 0
    }
    
    class func deleteAllNFCRow(tblName:String, id: Int) -> Bool {
        
        let query = "DELETE FROM \(tblName) WHERE \(kid) = '\(id)'"
        let rcs = SQLiteDB.shared.execute(sql: query)
        return rcs != 0 ? true : false
    }
    
    class func deleteAllNFC(tblName:String) -> Bool {
        
        let query = "DELETE FROM \(tblName)"
        let rcs = SQLiteDB.shared.execute(sql: query)
        return rcs != 0 ? true : false
    }
}
