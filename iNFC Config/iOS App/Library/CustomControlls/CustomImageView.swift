//
//  CustomImageView.swift
//  CustomClasses
//
//  Created by Nikhil Jobanputra on 25/04/20..
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

@IBDesignable
class CustomImageView: UIImageView {
    
    @IBInspectable var IsRound: Bool = false {
        didSet{
            if IsRound {
                layer.cornerRadius = (layer.frame.size.width + layer.frame.size.height) / 4
                layer.masksToBounds = true
            }
        }
    }
    @IBInspectable var BordeHeight: CGFloat = 0.0 {
        didSet{
            
            layer.borderWidth = BordeHeight

        }
    }
    @IBInspectable var BordeColor: UIColor = .clear {
        didSet{
            layer.borderColor = BordeColor.cgColor
        }
    }
    @IBInspectable var CornerRadius: CGFloat = 0.0 {
        didSet{
            layer.cornerRadius = CornerRadius
            layer.masksToBounds = true
        }
    }
    @IBInspectable var ShadowColor: UIColor = .clear {
        didSet{
            layer.shadowColor = ShadowColor.cgColor
        }
    }
    @IBInspectable var ShadowRadius: CGFloat = 0.0 {
        didSet{
            layer.shadowRadius = ShadowRadius
        }
    }
    @IBInspectable var ShadowOpacity: CGFloat = 0.0 {
        didSet{
            layer.shadowOpacity = Float(ShadowOpacity)
        }
    }
    @IBInspectable var ShadowOffset : CGSize = CGSize(width: 0, height: 0) {
        didSet{
            layer.shadowOffset = ShadowOffset
        }
    }
}
