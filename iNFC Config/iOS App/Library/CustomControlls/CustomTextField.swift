//
//  CustomTextFields.swift
//  CustomClasses
//
//  Created by Nikhil Jobanputra on 25/04/20..
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTextFields: UITextField {
    
    @IBInspectable var BordeHeight: CGFloat = 0.0 {
        didSet{
            layer.borderWidth = BordeHeight
        }
    }
    @IBInspectable var BordeColor: UIColor = .clear {
        didSet{
            layer.borderColor = BordeColor.cgColor
        }
    }
    @IBInspectable var CornerRadius: CGFloat = 0.0 {
        didSet{
            layer.cornerRadius = CornerRadius
            //layer.masksToBounds = true
        }
    }
    @IBInspectable var ShadowColor: UIColor = .clear {
        didSet{
            layer.shadowColor = ShadowColor.cgColor
        }
    }
    @IBInspectable var ShadowRadius: CGFloat = 0.0 {
        didSet{
            layer.shadowRadius = ShadowRadius
        }
    }
    @IBInspectable var ShadowOpacity: CGFloat = 0.0 {
        didSet{
            layer.shadowOpacity = Float(ShadowOpacity)
        }
    }
    @IBInspectable var ShadowOffset : CGSize = CGSize(width: 0, height: 0) {
        didSet{
            layer.shadowOffset = ShadowOffset
        }
    }
    @IBInspectable var LeftPadding: CGFloat = 0.0 {
        didSet {
            let leftview = UIView(frame: CGRect(x: 0, y: 0, width: Int(LeftPadding), height: Int(frame.size.height)))
            leftView = leftview
            leftViewMode = .always
        }
    }
    @IBInspectable var RightPadding: CGFloat = 0.0 {
        didSet {
            let rightview = UIView(frame: CGRect(x: 0, y: 0, width: Int(LeftPadding), height: Int(frame.size.height)))
            rightView = rightview
            rightViewMode = .always
        }
    }
    @IBInspectable var placeHolderColor: UIColor? {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: placeHolderColor!])
        }
    }
    @IBInspectable var CursorColor : UIColor? {
        didSet {
            self.tintColor = CursorColor
        }
    }
    @IBInspectable var bottomBorderHeight: CGFloat = 0.0
    @IBInspectable var bottomBorderColor: UIColor = .clear
    
    @IBInspectable var topBorderHeight: CGFloat = 0.0
    @IBInspectable var topBorderColor: UIColor = .clear
    
    @IBInspectable var leftBorderHeight: CGFloat = 0.0
    @IBInspectable var leftBorderColor: UIColor = .clear
    
    @IBInspectable var rightBorderHeight: CGFloat = 0.0
    @IBInspectable var rightBorderColor: UIColor = .clear
    
    override func layoutSubviews() {
        super.layoutSubviews()
    
        if bottomBorderHeight > 0 {
    
            let label = UILabel()
            label.backgroundColor = bottomBorderColor
            label.layer.cornerRadius = CornerRadius
            label.frame = CGRect(x: 0, y: self.bounds.size.height - bottomBorderHeight, width: self.bounds.size.width, height: bottomBorderHeight)
            
            addSubview(label)
            
        }
        if topBorderHeight > 0 {
            
            let label = UILabel()
            label.backgroundColor = topBorderColor
            label.layer.cornerRadius = CornerRadius
            label.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: topBorderHeight)
            
            addSubview(label)
            
        }
        if leftBorderHeight > 0 {
            
            let label = UILabel()
            label.backgroundColor = leftBorderColor
            label.layer.cornerRadius = CornerRadius
            label.frame = CGRect(x: 0, y: 0, width: leftBorderHeight, height: bounds.size.height)
            
            addSubview(label)
        }
        if rightBorderHeight > 0 {
            
            let label = UILabel()
            label.backgroundColor = rightBorderColor
            label.layer.cornerRadius = CornerRadius
            label.frame = CGRect(x: bounds.size.width - rightBorderHeight, y: 0, width: rightBorderHeight, height: bounds.size.height)
            
            addSubview(label)
        }
    }
}
