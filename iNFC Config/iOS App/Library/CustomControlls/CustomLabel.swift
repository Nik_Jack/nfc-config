//
//  CustomLabel.swift
//  CustomClasses
//
//  Created by Nikhil Jobanputra on 25/04/20..
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import UIKit

@IBDesignable
class CustomLabel: UILabel {
    
    @IBInspectable var IsRound: Bool = false {
        didSet{
            if IsRound {
                layer.cornerRadius = (layer.frame.size.width + layer.frame.size.height) / 4
                //layer.masksToBounds = true
            }
        }
    }
    @IBInspectable var BordeHeight: CGFloat = 0.0 {
        didSet{
            layer.borderWidth = BordeHeight
        }
    }
    @IBInspectable var BordeColor: UIColor = .clear {
        didSet{
            layer.borderColor = BordeColor.cgColor
        }
    }
    @IBInspectable var CornerRadius: CGFloat = 0.0 {
        didSet{
            layer.cornerRadius = CornerRadius
            layer.masksToBounds = true
        }
    }
    @IBInspectable var topInset: CGFloat = 0.0
    @IBInspectable var bottomInset: CGFloat = 0.0
    @IBInspectable var leftInset: CGFloat = 0.0
    @IBInspectable var rightInset: CGFloat = 0.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
        
    }
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
    @IBInspectable var MarqueeText: Bool = false {
        didSet{
            if MarqueeText {
                
                UIView.animate(withDuration: 5.0, delay: 0, options: ([.curveLinear, .repeat]), animations: {() -> Void in
                
                    self.frame.origin.x = -(self.superview?.frame.size.width)! + -(self.superview?.frame.size.width)! 
                    //self.center = CGPoint(x: self.frame.origin.x - self.frame.size.width - self.frame.origin.x, y: self.center.y)
                }, completion:  { _ in })
            }
        }
    }
}
