//
//  NFCReader.swift
//  NFC Config
//
//  Created by Nikhil Jobnaputra on 25/04/20.
//  Copyright © 2020 Nikhil Jobnaputra. All rights reserved.
//

import Foundation
import CoreNFC

let nfcReader = NFCReader()

class NFCReader: NSObject {
    
    var session: NFCNDEFReaderSession!
    typealias complitionBlock = ((String?, Error?) -> Void)
    var nfc: complitionBlock!
    var alertMsg:String = ""
    var isDeviceSupportNFCReading = false
    
    private var isReadValue:Bool!
    
    var arrofData:[UserData] = []
    
    override init() {
        super.init()
        isDeviceSupportNFCReading = NFCNDEFReaderSession.readingAvailable
    }
    
    private func StartScan(compilation: @escaping complitionBlock) {
        
        session = NFCNDEFReaderSession(delegate: self, queue: DispatchQueue.main, invalidateAfterFirstRead: false)
        session.alertMessage = alertMsg
        session?.begin()
        
        nfc = compilation
    }
    
    func writeNFC(arrofwriteData:[UserData],compilation: @escaping complitionBlock) {
        
        isReadValue = false
        arrofData = arrofwriteData
        StartScan(compilation: compilation)
    }
    
    func readNFC(compilation: @escaping complitionBlock){
        isReadValue = true
        
        StartScan(compilation: compilation)
    }
    
    private func readNFC(messages:NFCNDEFMessage,error:Error?) {
        
        var result = ""
        
        func parseURINFC(_ data: Data) -> String? {
            
            let prefix = data.prefix(1)
            let rest = data.dropFirst(1)
            
            switch prefix {
                
            case Data(bytes: [0x00]):
                return nil
            case Data(bytes: [0x01]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "http://www." + restString
            case Data(bytes: [0x02]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "https://www." + restString
            case Data(bytes: [0x03]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "http://" + restString
            case Data(bytes: [0x04]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "https://" + restString
            case Data(bytes: [0x05]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "tel://" + restString
                
            case Data(bytes: [0x06]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "mailto://" + restString
            case Data(bytes: [0x07]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "ftp://anonymous:anonymous@" + restString
            case Data(bytes: [0x08]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "ftp://ftp." + restString
            case Data(bytes: [0x09]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "ftps://" + restString
            case Data(bytes: [0x0A]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "sftp://" + restString
            case Data(bytes: [0x0B]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "smb://" + restString
                
                
            case Data(bytes: [0x0C]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "nfs://" + restString
            case Data(bytes: [0x0D]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "ftp://" + restString
            case Data(bytes: [0x0E]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "dav://" + restString
            case Data(bytes: [0x0F]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "news:" + restString
            case Data(bytes: [0x10]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "telnet://" + restString
            case Data(bytes: [0x11]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "imap:" + restString
                
            case Data(bytes: [0x12]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "rtsp://" + restString
            case Data(bytes: [0x13]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:" + restString
            case Data(bytes: [0x14]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "pop:" + restString
            case Data(bytes: [0x15]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "sip:" + restString
            case Data(bytes: [0x16]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "sips:" + restString
            case Data(bytes: [0x17]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "tftp:" + restString
                
            case Data(bytes: [0x18]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "btspp://" + restString
            case Data(bytes: [0x19]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "btl2cap://" + restString
            case Data(bytes: [0x1A]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "btgoep://" + restString
            case Data(bytes: [0x1B]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "tcpobex://" + restString
            case Data(bytes: [0x1C]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "irdaobex://" + restString
            case Data(bytes: [0x1D]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "file://" + restString
                
            case Data(bytes: [0x1E]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:epc:id:" + restString
            case Data(bytes: [0x1F]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:epc:tag:" + restString
            case Data(bytes: [0x20]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:epc:pat:" + restString
            case Data(bytes: [0x21]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:epc:raw:" + restString
            case Data(bytes: [0x22]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:epc:" + restString
            case Data(bytes: [0x23]):
                guard let restString = String(data: rest, encoding: .utf8) else { return nil }
                return "urn:nfc:" + restString
                
            default:
                return nil
            }
        }
        
        func parseData(typestr:String,pay:[UInt8],payloadData:NFCNDEFPayload) {
            
            var paydata = pay
            
            if typestr == "U", let purl = parseURINFC(payloadData.payload) {
                
                print("NFC URL : " + purl)
                result = purl
                
            } else {
                
                if paydata[0] == 0 { // Check first byte is '\0' or 0 than it remove
                    paydata.removeFirst()
                }
                if paydata[0] == 2 { // Check first byte is 'Space' " " than it remove
                    paydata.removeFirst()
                }
                if paydata[0] == 101 { // Check first byte is 'e' than it remove
                    paydata.removeFirst()
                }
                if paydata[0] == 110 { // Check first byte is 'n' than it remove
                    paydata.removeFirst()
                }
                result += String(bytes: paydata, encoding: .utf8) ?? ""
                print("NFC TEXT : " + result)
            }
        }
        
        
        
        for payload in messages.records {
            
            var pay = [UInt8](payload.payload)
            
            if let typeString = String(data: payload.type, encoding: .utf8) {
                print("Type String : \(typeString)")
                
                switch payload.typeNameFormat {
                case .nfcWellKnown:
                    
                    if #available(iOS 13.0, *) {
                        
                        if let url = payload.wellKnownTypeURIPayload() {
                            result = url.absoluteString
                        } else if typeString == "T" {
                            
                            let url = payload.wellKnownTypeTextPayload()
                            result = url.0 ?? String(bytes: pay, encoding: .utf8) ?? ""
                        } else {
                            parseData(typestr: typeString, pay: pay, payloadData: payload)
                        }
                        
                    } else {
                        parseData(typestr: typeString, pay: pay, payloadData: payload)
                    }
                case .absoluteURI:
                    if let url = String(data: payload.payload, encoding: .utf8) {
                        result = url
                    }
                default :
                    if typeString == "U", let purl = parseURINFC(payload.payload) {
                        
                        print("NFC URL : " + purl)
                        result = purl
                        
                    } else {
                        
                        if pay[0] == 0 { // Check first byte is '\0' or 0 than it remove
                            pay.removeFirst()
                        }
                        if pay[0] == 2 { // Check first byte is 'Space' " " than it remove
                            pay.removeFirst()
                        }
                        if pay[0] == 101 { // Check first byte is 'e' than it remove
                            pay.removeFirst()
                        }
                        if pay[0] == 110 { // Check first byte is 'n' than it remove
                            pay.removeFirst()
                        }
                        result += String(bytes: pay, encoding: .utf8) ?? ""
                        print("NFC TEXT : " + result)
                    }
                    break
                }
                self.nfc(result, error)
            }
        }
    }
}

//MARK:-  NFCNDEFReaderSessionDelegate Methods
extension NFCReader: NFCNDEFReaderSessionDelegate {
    
    internal func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        //        nfc(nil,error)
        print(error.localizedDescription)
    }
    
    
    @available(iOS 13.0, *)
    
    internal func readerSession(_ session: NFCNDEFReaderSession, didDetect tags: [NFCNDEFTag]) {
        
        let tag = tags.first!
        // 3
        
        session.connect(to: tag) { (error: Error?) in
            if error != nil {
                session.restartPolling()
            }
        }
        
        // 4
        tag.queryNDEFStatus() { (status: NFCNDEFStatus, capacity: Int, error: Error?) in
            
            if error != nil {
                session.invalidate()
                return
            }
            
            if status == .readOnly {
                
                tag.readNDEF { (messages, error) in
                    if error != nil {
                        print(error!.localizedDescription)
                        
                    } else {
                        self.readNFC(messages: messages!, error: error)
                        session.invalidate()
                    }
                }
                session.invalidate()
            } else if status == .readWrite {
                
                if self.isReadValue {
                    
                    tag.readNDEF { (messages, error) in
                        if error != nil {
                            print(error!.localizedDescription)
                            
                        } else {
                            
                            if messages != nil {
                                
                                self.readNFC(messages: messages!, error: error)
                                session.invalidate()
                            }
                        }
                    }
                } else {
                    
                    var payload:[NFCNDEFPayload] = []
                    
                    for data in self.arrofData {
                        
                        switch data.type {
                            
                        case writeType.Shortcut.type():
                            //payload = NFCNDEFPayload.wellKnownTypeURIPayload(string:data.WriteDataString ?? "")
                            break
                            
                        case writeType.Weblink.type():
                            payload.append(NFCNDEFPayload.wellKnownTypeURIPayload(string:data.writestring ?? "")!)
                            break
                            
                        case writeType.Text.type():
                            
                            payload.append(NFCNDEFPayload.wellKnownTypeTextPayload(string:data.writestring ?? "", locale: Locale.init(identifier: "en"))!)
                            break
                            
                        case writeType.TelephoneNumber.type():
                            payload.append(NFCNDEFPayload.wellKnownTypeTextPayload(string:data.writestring ?? "", locale: Locale.init(identifier: "en"))!)
                            break
                            
                        case writeType.SMS.type():
                            payload.append(NFCNDEFPayload.wellKnownTypeTextPayload(string:data.writestring ?? "", locale: Locale.init(identifier: "en"))!)
                            break
                            
                        case writeType.BusinessCard.type():
                            // payload = NFCNDEFPayload.wellKnownTypeTextPayload(string:data.WriteDataString ?? "", locale: Locale.init(identifier: "en"))
                            break
                            
                        case writeType.Location.type():
                             payload.append(NFCNDEFPayload.wellKnownTypeTextPayload(string:data.writestring ?? "", locale: Locale.init(identifier: "en"))!)
                            break
                            
                        default:
                            break
                        }
                    }
                    
                    let myMessage = NFCNDEFMessage(records: payload)
                    
                    tag.writeNDEF(myMessage) { (error: Error?) in
                        if error != nil {
                            session.invalidate(errorMessage: "Update tag failed. Please try again.")
                            self.nfc(session.alertMessage, error)
                        } else {
                            
                            session.alertMessage = "Profile written to the NFC tag successfully!"
                            // 6
                            session.invalidate()
                            self.nfc(session.alertMessage, nil)
                        }
                        session.invalidate()
                    }
                }
            } else {
                session.invalidate()
            }
        }
    }
    
    internal func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        
        readNFC(messages: messages[0], error: nil)
        
    }
}

